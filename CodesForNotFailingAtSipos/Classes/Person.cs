﻿using CodesForNotFailingAtSipos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodesForNotFailingAtSipos.Classes
{
    class Person : IMyObject
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
