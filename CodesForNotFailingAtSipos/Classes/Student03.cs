﻿using CodesForNotFailingAtSipos.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodesForNotFailingAtSipos.Classes
{
    class Student03 : Person
    {
        [CheckLength(MaxLength = 10)]
        public string Email { get; set; }

        public string NeptunID { get; set; }

        public int Credits { get; set; }
    }
}
