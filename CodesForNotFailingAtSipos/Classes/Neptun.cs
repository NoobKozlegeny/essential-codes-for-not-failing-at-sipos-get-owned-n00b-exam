﻿using CodesForNotFailingAtSipos.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodesForNotFailingAtSipos.Classes
{
    class Neptun
    {
        /// <summary>
        /// Ez egy magyarázat.
        /// </summary>
        public int ActiveStudentNumber { get; set; }
        
        [Obsolete("Use the newer one")] //Kijelzi, hogy már régi és érdemes újabbat használni
        public void EnrollStudent() { }

        [HelpText(Stuff = "Hello there boi")]
        public void NEW_EnrollStudent() { }
    }
}
