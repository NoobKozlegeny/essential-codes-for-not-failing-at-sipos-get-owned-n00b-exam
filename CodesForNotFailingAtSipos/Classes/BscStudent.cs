﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodesForNotFailingAtSipos.Classes
{
    class BscStudent : Student03
    {
        public int EnrollmentYear; // field, not property

        private int ActiveSemesters_1; // private is not visible
        public int ActiveSemesters_2;
        public int ActiveSemesters_3;
        public int ActiveSemesters_4;

        public string Greeting()
        {
            return "Hi! I'm " + Name + ", nice to meet you!";
        }
    }
}
