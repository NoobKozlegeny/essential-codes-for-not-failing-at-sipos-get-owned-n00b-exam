﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodesForNotFailingAtSipos.Attributes
{
    class HelpTextAttribute : Attribute
    {
        public string Stuff { get; set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    class OnlyClassAttribute : Attribute { }

    [AttributeUsage(AttributeTargets.Method)]
    class OnlyMethodAttribute : Attribute { }

    [AttributeUsage(AttributeTargets.Property)]
    class CheckLengthAttribute : Attribute
    {
        public int MaxLength { get; set; }
    }
}
