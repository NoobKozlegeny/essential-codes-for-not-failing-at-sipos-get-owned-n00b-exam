﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodesForNotFailingAtSipos.Classes
{
    class Car : IComparable
    {
        public string Model { get; set; }
        public int Mileage { get; set; }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Car car = obj as Car;
            if (car != null)
                return Mileage.CompareTo(car.Mileage);
            else
                throw new ArgumentException("Object is not a Car");
        }
    }
}
