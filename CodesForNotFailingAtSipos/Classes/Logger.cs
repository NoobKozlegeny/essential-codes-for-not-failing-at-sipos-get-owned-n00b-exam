﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodesForNotFailingAtSipos.Classes
{
    class Logger
    {
        Action<string> LoggerMethods = null; //Delegált létrehozása, amibe feltöltjük a metódusokat

        public void AddLogMethod(Action<string> methodToAdd) //Metódusok hozzáadása, paraméterként szintén delegáltat adtam, mert csak így lehet metódust átadni.
        {
            LoggerMethods += methodToAdd;
        }

        public void Log(string message) //Delegált lefuttatása, duhh
        {
            LoggerMethods?.Invoke(message);
        }
    }
}
