﻿using CodesForNotFailingAtSipos.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using System.Reflection;
using CodesForNotFailingAtSipos.Interfaces;

namespace CodesForNotFailingAtSipos
{
    class Program
    {
        public delegate string Methods(int a, int b);
        static Random r = new Random();
        static void Main(string[] args)
        {
            //MINFO01();
            //MINFO02();
            MINFO03();
        }
        //HFT MINFO 01 - DELEGATE
        static void MINFO01()
        {
            //Delegált alapok
            Console.WriteLine("Delegate recap:");
            Methods methods = new Methods(Method1);
            methods += Method2;

            methods(10, 2); //Ne ezt használd, mert ha üres akkor NullException.

            methods -= Method1;

            methods?.Invoke(10, 2); //Ezt told, mert ez ellenőrzi, hogy null-e a delegált.

            //Névtelen függvények/metódusok: Ha csak egyszer kellő függvények kellenek
            Console.WriteLine("Anonym methods:");
            methods += delegate (int a, int b)
            {
                Console.WriteLine($"\tI sexed your mom {a} times, and your dad {b} times.");
                return $"\tI sexed your mom {a} times, and your dad {b} times.";
            };

            methods?.Invoke(69, 42);
            //Lambda: Ez is névtelen függvény
            Console.WriteLine("LINQ:");
            List<int> numberList = new List<int>();
            Random r = new Random();
            for (int i = 0; i < 10; i++)
            {
                numberList.Add(r.Next(0, 100));
            }

            Console.WriteLine("\t" + numberList.Find(x => x > 75)); //Legelső szám ami nagyobb, mint 75
            Console.Write("\t");
            numberList.OrderBy(x => x).ToList().ForEach(x => Console.Write(x + ", ")); //Sorrendbe helyezés és kiírás
            Console.WriteLine();
            //...

            //Action, Func, Predicate
            Console.WriteLine("Action, Func, Predicate:");
            /*
            Action: Delegált/pointer egy metódushoz, ami akármennyi paraméter kaphat, de nem küldd vissza semmit se.
                Végülis egy void delegált, ami void metódust/metódusokkal dolgozik.
            Func: Delegált/pointer egy metódushoz, ami akármennyi paraméter kaphat és van visszatérési értéke.
                Végülis egy return-ös delegált, ami return-ös metódust/metódusokkal dolgozik.
                Legutolsó/legjobboldalibb paraméter a return típusa.
            Predicate: Func, de csakis bool értéket küld vissza.
             */
            Action<string> act = x => Console.WriteLine($"\tSup {x} are you an angel? Because I wanna die."); //Action feltöltése 1. metódussal
            act += x => Console.WriteLine($"\t{x} is kinda sus."); //2. metódus feltöltése
            act?.Invoke("Sanyi");

            Func<int, int, string> func1 = Method1; //Func feltöltése 1. metódussal
            func1 = (x, y) => $"\tMultiplication: {x * y}"; //2. metódus feltöltése
            string func1Result = func1?.Invoke(10, 2);
            Console.WriteLine(func1Result);

            Predicate<int> predicate = x => x % 2 == 0;
            Console.WriteLine("\t" + predicate(10));
            predicate += x => WillYouBeEverHappy(x);
            Console.WriteLine("\t" + predicate(3));

            //(Array.)Sort rendezés osztállyal
            Console.WriteLine("(Array.)Sort ordering/comparison:");
            List<Car> cars = new List<Car>()
            {
                new Car { Model="Yaris", Mileage=250032 },
                new Car { Model="Opel", Mileage=220320 },
                new Car { Model="MX-5", Mileage=320560 },
                new Car { Model="Maserati", Mileage=12027 },
                new Car { Model="Beetle", Mileage=67894 },
            };

            cars.Sort(CarComparer); //Igazából kb ezt mutatta meg Sipos csak Array.Sort-al,
                                    //de szerintem ez a delegált használata(CarComparer) totál fölösleges,
                                    //mert enélkül is lefut sikeresen a rendezés. => cars.Sort();
            cars.ForEach(x => Console.WriteLine($"\tModel: {x.Model}, Mileage: {x.Mileage}"));

            //Feladat: https://youtu.be/3-sV8CH22N4?t=2310
            Logger logger = new Logger();
            StreamWriter sw = new StreamWriter("feladatLogger.txt");

            logger.AddLogMethod(x => Console.WriteLine(x));
            logger.AddLogMethod(x => sw.WriteLine(x));

            logger.Log("Összekagáltam magam.");
            sw.Close();
        }
        static bool WillYouBeEverHappy(int a)
        {
            if (a % 2 == 0) { return true; }
            else { return false; }
        }
        static string Method1(int a, int b)
        {
            return $"\tThere's {a} amount of war crimes you have committed in {b} countries || ";
        }
        static string Method2(int a, int b)
        {
            return ("\t"+a*b).ToString();
        }
        static int CarComparer(Car x, Car y)
        {
            return x.CompareTo(y);
        }
        //MINFO 02 - LINQ & XML
        static void MINFO02()
        {
            List<Car> cars = new List<Car>()
            {
                new Car { Model="Yaris", Mileage=250032 },
                new Car { Model="Opel", Mileage=220320 },
                new Car { Model="MX-5", Mileage=320560 },
                new Car { Model="Maserati", Mileage=12027 },
                new Car { Model="Beetle", Mileage=67894 },
            };

            List<Student> students = new List<Student>
            {
                new Student { Name = "Toporgó Tamás" },
                new Student { Name = "Embertelen Elemér" },
                new Student { Name = "Xedik Xavér" },
                new Student { Name = "Kilencedik Klaudia" },
                new Student { Name = "Ketyós KlaUdIA" },
            };

            //Anonym classes and var
            Console.WriteLine("Var:");
            IEnumerable<string> queryWithoutVar = cars.Where(x => x.Mileage > 15000).Select(x=>x.Model); //Megadjuk a visszatérési típusát: IEnumerable<string>
            var queryWithVar = cars.Where(x => x.Mileage > 15000).Select(x => x.Model); //var-ba akármit beletehetünk. Ez akkor jó, ha nem tudjuk mi a visszatérési érték, vagy nem is érdekel minket.
            Process(queryWithoutVar);
            Process(queryWithVar);

            Console.WriteLine("Anonym class:");
            var anonClass = new
            {
                Name = "Anon53534335",
                IsIncel = true,
                Salary = 0
            };
            Console.WriteLine($"\t{anonClass.Name}, {anonClass.IsIncel}, {anonClass.Salary}");

            //LINQ
            //query syntax
            Console.WriteLine("Query syntax:");

            var queryEx1 = from x in cars
                     where x.Model.Contains("e")
                     select x.Model;
            Process(queryEx1);

            var queryEx2 = from x in cars
                     orderby x.Mileage
                     select x.Mileage;
            Process(queryEx2);
            //method syntax
            Console.WriteLine("Method syntax:");

            var methodEx1 = cars.Where(x => x.Model.Contains("e")).Select(x => x.Model);
            Process(methodEx1);

            var methodEx2 = cars.OrderBy(x => x.Mileage).Select(x=>x.Mileage);
            Process(methodEx2);

            //1. Feladat
            //Sipos megoldása
            Console.WriteLine("1. Feladat:");

            int count = students.Count(x => x.Name.ToUpper().Contains("KLAUDIA"));
            var klaudiasLINQ = students.Where(x => x.Name.ToUpper().Contains("KLAUDIA"));
            Student[] klaudias1 = new Student[count];
            
            int index = 0;
            foreach (var item in klaudiasLINQ)
            {
                klaudias1[index] = item;
                index++;
            }
            Process(klaudias1);
            //Egyszerűbb megoldás
            Student[] klaudias2 = students.Where(x => x.Name.ToUpper().Contains("KLAUDIA")).ToArray();

            //2. Feladat
            Console.WriteLine("2. Feladat:");

            Predicate<int> statusRandomizer = x => { return x == 0; };

            foreach (var item in students)
            {
                item.Status = (bool)statusRandomizer?.Invoke(r.Next(0, 2));
                item.Age = r.Next(18, 80);
            }

            var m2 = students.Where(x => x.Age >= 20 && x.Age <= 50 && x.Status == false);
            Process(m2);

            //3. Feladat:
            Console.WriteLine("3. Feladat:");

            var m3 = students.Where(x => x.Status == true)
                .OrderBy(x => x.Name).Select(x => x.Name.ToUpper());
            Process(m3);

            //4. Feladat:
            Console.WriteLine("4. Feladat:");

            var m4 = students.GroupBy(x => x.Status);

            var q4 = from x in students
                     group x by x.Status into g
                     select new
                     {
                         KEY = g.Key,
                         COUNT = g.Count()
                     }; //KEY, COUNT lehet akármilyen nevű, mert ez egy anonim osztály

            foreach (var item in m4)
            {
                Console.WriteLine($"\tCsoport: {item.Key}, Db: {item.Count()}");
            }

            foreach (var item in q4)
            {
                Console.WriteLine($"\tCsoport: {item.KEY}, Db: {item.COUNT}");
            }

            //5. Feladat:
            Console.WriteLine("5. Feladat:");

            var m5 = students.Where(x => x.Name.ToUpper().Contains("E"))
                .Select(x=> new { x.Name, x.Age }).OrderBy(x=>x.Age);
            Process(m5);

            Func<bool, string> statusFunc = x => x ? "Kapcsolatban vagyok." : "Szingli vagyok.";

            var q5 = from x in students
                     where x.Name.ToUpper().Contains("E")
                     orderby x.Age
                     select new
                     {
                         NAME = x.Name,
                         AGE = x.Age,
                         STATUS = statusFunc?.Invoke(x.Status)
                     };
            Process(q5);

            //5.1 Feladat:
            Console.WriteLine("5.1 Feladat:");

            var q5_1 = from x in q5
                       group x by x.STATUS into g
                       select new
                       {
                           STATUS = g.Key,
                           AVG = Math.Round(g.Average(x => x.AGE),1),
                           COUNT = g.Count()
                       };
            Process(q5_1);

            //XML https://users.nik.uni-obuda.hu/siposm/db/workers.xml
            XDocument xdoc = XDocument.Load("https://users.nik.uni-obuda.hu/siposm/db/workers.xml");
            //Process(xdoc);

            //0. Feladat:
            Console.WriteLine("0. Feladat:");
            var task0 = from x in xdoc.Root.Elements("person")
                        select x.Element("name").Value;
            Process(task0);

            //1. Feladat:
            Console.WriteLine("1. Feladat:");
            var task1 = from x in xdoc.Root.Elements("person")
                        where x.Element("name").Value.ToUpper().Contains("TAMÁS")
                        select x.Element("name").Value;
            Process(task1);

            //2. Feladat:
            Console.WriteLine("2. Feladat:");
            var task2 = from x in xdoc.Root.Elements("person")
                        where x.Element("rank").Value.Contains("polihisztor")
                        select new
                        {
                            NAME = x.Element("name").Value,
                            EMAIL = x.Element("email").Value
                        };
            Process(task2);

            //3. Feladat:
            Console.WriteLine("3. Feladat:");
            var task3 = from x in xdoc.Root.Elements("person")
                        group x by x.Element("dept").Value into g
                        select new
                        {
                            KEY = g.Key,
                            COUNT = g.Count()
                        };
            Process(task3);
            #region Házi3-as Rendezése (Mindkettő jó)
            //task3 = from x in task3
            //        orderby x.COUNT
            //        select new
            //        {
            //            KEY = x.KEY,
            //            COUNT = x.COUNT
            //        };

            //task3 = task3.OrderBy(x => x.COUNT);
            #endregion

            //4. Feladat:
            Console.WriteLine("4. Feladat:");
            var task4 = from x in xdoc.Root.Elements("person")
                        where x.Element("dept").Value.Equals("Alkalmazott Informatikai Intézet")
                        group x by x.Element("dept").Value into g
                        select new
                        {
                            KEY = g.Key,
                            COUNT = g.Count()
                        };
            Process(task4);

            var task4HF = from x in task3
                          where x.KEY.Equals("Alkalmazott Informatikai Intézet")
                          select new
                          {
                              KEY = x.KEY,
                              COUNT = x.COUNT
                          };

            //5. Feladat:
            XElement element = new XElement("person",
                    new XAttribute("status","retarded"),
                    new XElement("name", 
                        new XAttribute("nickname", "The Serb"), "Cirill Boi"),
                    new XElement("emai", "cirill@gmail.com"),
                    new XElement("dept", 
                        new XAttribute("short", "OCS"), "Oktatási Csoport"),
                    new XElement("rank", "egyetemi docens"),
                    new XElement("phone", "+36 (1) 690-4201"),
                    new XElement("room", "BA.1.22")
                    );

            xdoc.Root.Add(element);
            xdoc.Save("_output.xml");

            //(6. Feladat) HF:
            XDocument xdoc2 = XDocument.Load("_output.xml");
            foreach (var item in xdoc2.Root.Elements("person"))
            {
                item.Element("name").Value = item.Element("name").Value.ToUpper();
            }

            xdoc2.Save("_outputUpper.xml");
        }
        static void Process<T>(IEnumerable<T> ts)
        {
            Console.Write("\t");
            foreach (var item in ts)
            {
                Console.Write($"{item}, ");
            }
            Console.WriteLine();
        }
        static void Process(XDocument xdoc)
        {
            var people = from x in xdoc.Root.Elements("person") //vagy xdoc.Root.Descendants("person")
                         select x;
            foreach (var item in people)
            {
                Console.WriteLine(item);
            }
        }
        //MINO 03 - REFLECTION & ATTRIBUTE
        static void MINFO03()
        {
            /*
            typeof: typeof(Osztály); Itt kell tudni az adott objektum típusát
            GetType: referencia.GetType(); Itt nem kell tudni az adott objektum típusát

            Type:
            PropertyInfo/MethodInfo/FieldInfo: Tulajdonság/Metódus/Mező tárolása.
            Activator.CreateInstance(bscStudType): Egy példányt létrehoz (Üres adatokkal)
             */
            Person p = new Person { Name = "Cotton Joe", ID = 1 };

            Type t = typeof(Person); //Megadja, hogy a Person Person típusú
            PropertyInfo[] infos = t.GetProperties(); //A típus tulajdonságait lementi. (Most Name, ID)

            foreach (var item in infos)
            {
                //Console.WriteLine("\tNAME: " + item); //Kiírja a tulajdonság nevét és milyen visszatérésű (System.String Name, Int32 ID)
                Console.WriteLine("\tNAME: "+item.Name);
                Console.WriteLine("\tNAME: " + item.GetValue(p)); //Konkrét példány értékeinek kiírása
            }

            PropertyInfo pi = t.GetProperty("Name"); //Adott típus, adott tulajdonságának kiszedése
            Console.WriteLine(pi.GetValue(p)); //Az egyik példány kiválasztott tulajdonságának értékének kiírása

            Type bscStudType = typeof(BscStudent);
            object instObj = Activator.CreateInstance(bscStudType); //Egy BscStudent példányt létrehoz.

            MethodInfo mi = bscStudType.GetMethod("Greeting"); //A típusnak az egyik metódusát kiválasztja.
            string returnedValue = mi.Invoke(instObj, null).ToString(); //Lefuttatjuk a kiválasztott metódust
            Console.WriteLine("\treturned value: " + returnedValue);

            //Generáljunk random típusokat az osztályok mentén
            //Reflexióval írjuk ki típusonként a metódusokat/tulajdonságokat/adattagokat
            Console.WriteLine("Random created types:");
            IMyObject[] students = new IMyObject[10];
            Random r = new Random();

            Func<Type> typeRandomizer = () => //Ez random létrehoz Student03, BscStudent és Person típusokat
            {
                int x = r.Next(0, 3);
                if (x == 0) { return typeof(Student03); }
                else if (x == 1) { return typeof(BscStudent); }
                else { return typeof(Person); }
            };

            Assembly assem = Assembly.GetExecutingAssembly(); //Ez csak kell.

            for (int i = 0; i < students.Length; i++)
            {
                //Ez létrehozza a random 3 típus közül valamelyiket, példányosítja és IMyObject-re kasztolja,
                //mert fasztudja melyik típus lesz.
                IMyObject imo = (IMyObject)assem.CreateInstance(typeRandomizer().ToString());
                students[i] = imo;
                Console.WriteLine("\t"+imo);
            }

            //Annek megfelelően, hogy milyen a típus listázzuk ki
            //a tulajdonságokat, metódusokat és az adattagokat
            for (int i = 0; i < students.Length; i++)
            {
                Type ts = students[i].GetType(); //Típus kiszerzése
                PropertyInfo[] pis = ts.GetProperties(); //Tulajdonságok kiszerzése
                MethodInfo[] mis = ts.GetMethods(); //Metódusok kiszerzése
                FieldInfo[] fis = ts.GetFields(); //Mezők kiszerzése

                Console.WriteLine($"\t{students[i]}'s properties:");
                for (int j = 0; j < pis.Length; j++)
                {
                    Console.WriteLine($"\t\t{pis[j].Name}");
                }
                Console.WriteLine($"\t{students[i]}'s methods:");
                for (int j = 0; j < mis.Length; j++)
                {
                    Console.WriteLine($"\t\t{mis[j].Name}");
                }
                Console.WriteLine($"\t{students[i]}'s fields:");
                for (int j = 0; j < fis.Length; j++)
                {
                    Console.WriteLine($"\t\t{fis[j].Name}");
                }
            }
        }
    }
}
